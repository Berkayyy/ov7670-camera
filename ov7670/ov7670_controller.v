`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.12.2021 19:34:06
// Design Name: 
// Module Name: ov7670_controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ov7670_controller(
  input clk,
  input resend,
  output reg config_finished,
  output sioc,
  inout siod,
  output reg reset,
  output reg pwdn,
  output reg xclk );
  
  reg sys_clk;
  wire [15:0] command;
  wire finished;
  wire taken;
  reg send;
  
  initial sys_clk = 0;
  
  localparam [7:0] camera_address = 8'h42;

always@*
begin
     config_finished <= finished;
     send <= ~finished;
   end
   
   i2c_sender Inst_i2c_sender (
         .clk(clk),
         .taken(taken),
         .siod(siod),
         .sioc(sioc),
         .send(send),
         .id(camera_address),
         .register(command[15:8]),
         .value(command[7:0]));

always@*
begin
     reset <= 1'b1;
     pwdn  <= 1'b0;
     xclk  <= sys_clk;
end

   ov7670_registers Inst_ov7670_registers (
         .clk(clk),
         .advance(taken),
         .command(command),
         .finished(finished),
         .resend(resend));
always@(posedge clk)
begin
         sys_clk <= ~sys_clk;
 end 
         
endmodule
