## FPGA4student.com: Interfacing Basys 3 FPGA with OV7670 Camera
## Pin assignment

## Clock signal
set_property PACKAGE_PIN W5 [get_ports clk]							
	set_property IOSTANDARD LVCMOS33 [get_ports clk]
	create_clock -add -name sys_clk_pin -period 10.00 -waveform {0 5} [get_ports clk]

    ##VGA Connector
    set_property PACKAGE_PIN G19 [get_ports {vga_red[0]}]                
        set_property IOSTANDARD LVCMOS33 [get_ports {vga_red[0]}]
    set_property PACKAGE_PIN H19 [get_ports {vga_red[1]}]                
        set_property IOSTANDARD LVCMOS33 [get_ports {vga_red[1]}]
    set_property PACKAGE_PIN J19 [get_ports {vga_red[2]}]                
        set_property IOSTANDARD LVCMOS33 [get_ports {vga_red[2]}]
    set_property PACKAGE_PIN N19 [get_ports {vga_red[3]}]                
        set_property IOSTANDARD LVCMOS33 [get_ports {vga_red[3]}]
    set_property PACKAGE_PIN N18 [get_ports {vga_blue[0]}]                
        set_property IOSTANDARD LVCMOS33 [get_ports {vga_blue[0]}]
    set_property PACKAGE_PIN L18 [get_ports {vga_blue[1]}]                
        set_property IOSTANDARD LVCMOS33 [get_ports {vga_blue[1]}]
    set_property PACKAGE_PIN K18 [get_ports {vga_blue[2]}]                
        set_property IOSTANDARD LVCMOS33 [get_ports {vga_blue[2]}]
    set_property PACKAGE_PIN J18 [get_ports {vga_blue[3]}]                
        set_property IOSTANDARD LVCMOS33 [get_ports {vga_blue[3]}]
    set_property PACKAGE_PIN J17 [get_ports {vga_green[0]}]                
        set_property IOSTANDARD LVCMOS33 [get_ports {vga_green[0]}]
    set_property PACKAGE_PIN H17 [get_ports {vga_green[1]}]                
        set_property IOSTANDARD LVCMOS33 [get_ports {vga_green[1]}]
    set_property PACKAGE_PIN G17 [get_ports {vga_green[2]}]                
        set_property IOSTANDARD LVCMOS33 [get_ports {vga_green[2]}]
    set_property PACKAGE_PIN D17 [get_ports {vga_green[3]}]                
        set_property IOSTANDARD LVCMOS33 [get_ports {vga_green[3]}]
    set_property PACKAGE_PIN P19 [get_ports hsync]                        
        set_property IOSTANDARD LVCMOS33 [get_ports hsync]
    set_property PACKAGE_PIN R19 [get_ports vga_vsync]                        
        set_property IOSTANDARD LVCMOS33 [get_ports vsync]

## LEDs
set_property PACKAGE_PIN U16 [get_ports {config_finished}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {config_finished}]
					
##Buttons
#set_property PACKAGE_PIN U18 [get_ports btnc]						
#	set_property IOSTANDARD LVCMOS33 [get_ports btnc]
#set_property PACKAGE_PIN W19 [get_ports btn1]                        
#     set_property IOSTANDARD LVCMOS33 [get_ports btnl]
#set_property PACKAGE_PIN T17 [get_ports btnr]						
#         set_property IOSTANDARD LVCMOS33 [get_ports btnr]
## OV7670 Camera header pins

##Pmod Header JB
##Sch name = JB1
set_property PACKAGE_PIN A14 [get_ports {pwdn}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {pwdn}]
##Sch name = JB2
set_property PACKAGE_PIN A16 [get_ports {m_axis_tdata[0]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {m_axis_tdata[0]}]
##Sch name = JB3
set_property PACKAGE_PIN B15 [get_ports {m_axis_tdata[2]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {m_axis_tdata[2]}]
##Sch name = JB4
set_property PACKAGE_PIN B16 [get_ports {m_axis_tdata[4]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {m_axis_tdata[4]}]
##Sch name = JB7
set_property PACKAGE_PIN A15 [get_ports {reset}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {reset}]
##Sch name = JB8
set_property PACKAGE_PIN A17 [get_ports {m_axis_tdata[1]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {m_axis_tdata[1]}]
##Sch name = JB9
set_property PACKAGE_PIN C15 [get_ports {m_axis_tdata[3]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {m_axis_tdata[3]}]
##Sch name = JB10 
set_property PACKAGE_PIN C16 [get_ports {m_axis_tdata[5]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {m_axis_tdata[5]}]
  

##Pmod Header JC
##Sch name = JC1
set_property PACKAGE_PIN K17 [get_ports {m_axis_tdata[6]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {m_axis_tdata[6]}]
##Sch name = JC2
set_property PACKAGE_PIN M18 [get_ports xclk]					
	set_property IOSTANDARD LVCMOS33 [get_ports xclk]
##Sch name = JC3
set_property PACKAGE_PIN N17 [get_ports href]					
	set_property IOSTANDARD LVCMOS33 [get_ports href]
##Sch name = JC4
set_property PACKAGE_PIN P18 [get_ports siod]					
	set_property IOSTANDARD LVCMOS33 [get_ports siod]
	set_property PULLUP TRUE [get_ports siod]
##Sch name = JC7
set_property PACKAGE_PIN L17 [get_ports {m_axis_tdata[7]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {m_axis_tdata[7]}]
##Sch name = JC8
set_property PACKAGE_PIN M19 [get_ports pclk]					
	set_property IOSTANDARD LVCMOS33 [get_ports pclk]
    set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets {pclk_IBUF}]
##Sch name = JC9
set_property PACKAGE_PIN P17 [get_ports vsync]					
	set_property IOSTANDARD LVCMOS33 [get_ports vsync]
##Sch name = JC10
set_property PACKAGE_PIN R18 [get_ports sioc]					
	set_property IOSTANDARD LVCMOS33 [get_ports sioc]

