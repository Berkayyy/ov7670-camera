`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.12.2021 20:01:36
// Design Name: 
// Module Name: ov7670_axi_stream_capture
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ov7670_axi_stream_capture(
        input pclk,
        input vsync,
        input href,
        input [7:0] d,
        output reg [31:0] m_axis_tdata,
        output reg m_axis_tvalid,
        input m_axis_tready,
        output reg m_axis_tlast,
        output reg m_axis_tuser,
        output reg aclk );
    
    reg [15:0] d_latch;
    reg [18:0] address;
    reg [1:0] line;
    reg [6:0] href_last;
    reg we_reg;
    reg href_hold;
    reg latched_vsync;
    reg latched_href;
    reg [7:0] latched_d;
    reg sof;
    reg eol;
    
      initial
      begin
            d_latch <= 0;
            address <= 0;
            line <= 0;
            href_last <= 0;
            we_reg <= 0;
            href_hold <= 0;
            latched_vsync <= 0;
            latched_href <= 0;
            sof <= 0;
            eol <= 0;
      end
      
      always@*
      begin
            m_axis_tdata <= {8'b11111111, d_latch[4:0], d_latch[0],
d_latch[0], d_latch[0], d_latch[10:5], d_latch[5], d_latch[5],
d_latch[15:11], d_latch[11], d_latch[11], d_latch[11]};
            m_axis_tvalid <= we_reg;
            m_axis_tlast <= eol;
            aclk <= ~pclk;
      end
      
            always@ (posedge pclk)
            begin
                  if (we_reg == 1'b1)
                  begin
                        address <= address + 1;
                  end
                  
                  if (href_hold == 1'b0 && latched_href == 1'b1)
                  begin
                        case (line)
                             2'b00: line <= 2'b01;
                             2'b01: line <= 2'b10;
                             2'b10: line <= 2'b11;
                             default: line <= 2'b00;
                        endcase
                  end
                  href_hold <= latched_href;
                  
                  // Capturing the data from the camera
                  if (latched_href == 1'b1)
                  begin
                        d_latch <= {d_latch[7:0], latched_d};
                  end
                  we_reg <= 1'b0;
                  
                  // Is a new screen about to start
                  if (latched_vsync == 1'b1)
                  begin
                        address <= 0;
                        href_last <= 0;
                        line <= 0;
                  end
                  
                  else
                  begin
                  
                        if (href_last [0] == 1'b1)
                        begin
                              we_reg <= 1'b1;
                              href_last <= 0;
                        end
                        else
                        begin
                              href_last <= {href_last[5:0],
latched_href};
                        end
                  end
                  
                  if(address % 640 == 639)
                  begin
                        eol <= 1'b1;
                  end
                  else
                  begin
                        eol <= 1'b0;
                  end
                  
                  if(address == 0)
                  begin      
                        sof <= 1'b1;
                  end
                  else
                  begin
                        sof <= 1'b0;
                  end  
            
             end
             
                  always@(negedge pclk)
                  begin
                        latched_d     <= d;
                        latched_href  <= href;
                        latched_vsync <= vsync;
                  end
 
       
endmodule
