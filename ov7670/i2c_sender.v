`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.12.2021 19:41:29
// Design Name: 
// Module Name: i2c_sender
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module i2c_sender( input clk,
                   inout siod,
                   output reg sioc,
                   output reg taken,
                   input send,
                   input [7:0] id,
                   input [7:0] register,
                   input [7:0] value ) ;
    reg SIOD;
    reg [7:0] divider;
    reg [31:0] busy_sr;
    reg [31:0] data_sr;
    
    initial divider = 8'b00000001;
    initial busy_sr = 0;
    initial data_sr = 32'hFFFFFFFF;
    assign siod = SIOD;
    
    always@(busy_sr, data_sr[31])
    begin
        if (busy_sr[11:10] == 2'b10 || busy_sr[20:19] == 2'b10 ||
busy_sr[29:28] == 2'b10)
            SIOD <= 1'bZ;
        else
            SIOD <= data_sr[31];
    end
    
    always@(posedge clk)
    begin
        taken <= 1'b0;
        if (busy_sr[31] == 1'b0)
        begin
            sioc <= 1'b1;
            if (send == 1'b1)
            begin
                if (divider == 8'b00000000)
                begin
                    data_sr <= {3'b100, id, 1'b0, register, 1'b0,
value, 1'b0, 2'b01};
                    busy_sr <= {3'b111, 9'b11111111, 9'b11111111,
9'b11111111, 2'b1};
                    taken <= 1'b1;
                end
                else
                begin
                  divider <= divider + 1;
                end
            end  
        end
        else
        begin
            case ({busy_sr[31:29], busy_sr[2:0]})
                6'b111111:
                    case (divider[7:6])
                        2'b00: sioc <= 1'b1;
                        2'b01: sioc <= 1'b1;
                        2'b10: sioc <= 1'b1;
                         default : sioc <= 1'b1;
                    endcase
                6'b111110:
                    case (divider[7:6])
                        2'b00: sioc <= 1'b1;
                        2'b01: sioc <= 1'b1;
                        2'b10: sioc <= 1'b1;
                         default : sioc <= 1'b1;
                    endcase
                6'b111100:
                    case (divider [7:6])
                        2'b00: sioc <= 1'b0;
                        2'b01: sioc <= 1'b0;
                        2'b10: sioc <= 1'b0;
                         default : sioc <= 1'b0;
                    endcase    
                6'b111000:
                    case (divider [7:6])
                        2'b00: sioc <= 1'b0;
                        2'b01: sioc <= 1'b1;
                        2'b10: sioc <= 1'b1;
                         default : sioc <= 1'b1;
                    endcase
                6'b100000:
                    case (divider [7:6])
                        2'b00: sioc <= 1'b1;
                        2'b01: sioc <= 1'b1;
                        2'b10: sioc <= 1'b1;    
                         default : sioc <= 1'b1;
                    endcase
                6'b00000:
                    case (divider [7:6])
                        2'b00: sioc <= 1'b1;
                        2'b01: sioc <= 1'b1;
                        2'b10: sioc <= 1'b1;
                         default : sioc <= 1'b1;
                    endcase
                default:
                    case (divider[7:6])
                        2'b00: sioc <= 1'b0;
                        2'b01: sioc <= 1'b1;
                        2'b10: sioc <= 1'b1;
                         default : sioc <= 1'b0;
                    endcase
            endcase
            end
                    if (divider == 8'b11111111)
                    begin
                        busy_sr <= {busy_sr[30:0], 1'b0};
                        data_sr <= {data_sr[30:0], 1'b1};
                        divider <= 0;
                    end
                    else
                    begin
                       divider <= divider +1;
                    end
                end             
      

endmodule




