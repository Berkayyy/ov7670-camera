`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.12.2021 20:59:33
// Design Name: 
// Module Name: ov7670_axi_stream_vga
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ov7670_axi_stream_vga(
        input clk25,
        output reg [4:0] vga_red,
        output reg [5:0] vga_green,
        output reg [4:0] vga_blue,
        output reg vga_hsync,
        output reg vga_vsync,
        input [31:0] s_axis_tdata,
        input s_axis_tvalid,
        output reg s_axis_tready,
        input s_axis_tlast,
        input s_axis_tuser );
        
      // Timing constants
      localparam hRez       = 640;
      localparam hStartSync = 640+16;
      localparam hEndSync   = 640+16+96;
      localparam hMaxCount  = 800;
      
      localparam vRez       = 480;
      localparam vStartSync = 480+10;
      localparam vEndSync   = 480+10+2;
      localparam vMaxCount  = 480+10+2+33;
      
      localparam hsync_active = 1'b0;
      localparam vsync_active = 1'b0;
      
      reg [9:0] hCounter;
      reg [9:0] vCounter;
      reg [18:0] address;
      reg [17:0] frame_addr;
      reg [15:0] frame_pixel;
      reg blank;
      
      reg we_reg;
      reg sof;
      reg eol;
      reg start;
       
       initial
       begin
             hCounter = 0;
             vCounter = 0;
             address = 0;
             frame_addr = 0;
           frame_pixel = 0;
             blank = 1'b1;
             we_reg = 0;
             sof = 0;
             eol = 0;
             start = 0;
       end
       
       always@*
       begin
             frame_pixel <= {s_axis_tdata[23:19],
  s_axis_tdata[15:10], s_axis_tdata [7:3]};
             we_reg <= s_axis_tvalid;
             eol <= s_axis_tlast;
             sof <= s_axis_tuser;
             frame_addr <= address[18:1];
      end
      
      always@(posedge clk25)
      begin
            // Count the lines and rows
        if (hCounter == hMaxCount-1)             //yatay s�f�rla
            begin
                  hCounter <= 0;
                  if (vCounter == vMaxCount-1)  //dikey s�f�rla
                  begin
                        s_axis_tready <= 1'b0;
                        vCounter <= 0;
                  end
                  else
                  begin
                        s_axis_tready <= 1'b1;
                        vCounter <= vCounter + 1; // dikey artt�r
                  end  
            end 
        else
            begin
           hCounter <= hCounter + 1;     //tarama i�in yatay artt�r
        end
        
        if (blank == 1'b0)
            begin
                  vga_red   <= frame_pixel[15:11];
                  vga_green <= frame_pixel[10:5];
                  vga_blue  <= frame_pixel[ 4:0];
            end  
        else
            begin
                  vga_red   <= 0;
                  vga_green <= 0;
                  vga_blue  <= 0;
        end
        
        if (we_reg == 1'b1)
        begin
              if (eol == 1'b1)
              begin
                    address <= 0;
                    start <= 1'b0;
              end
              else
              begin
                    if (sof == 1'b1)
                    begin
                          start <= 1'b1;
                    end  
              end
              
              if (start == 1'b1)
              begin
                    if (vCounter >= vRez )  //blank durumu
                    begin
                          address <= 0;
                          blank <= 1'b1;
                    end
                    else
                    begin
                          if (hCounter < 640)    //blank durumu
                          begin
                          blank <= 1'b0;
                          address <= address + 1;
                          end
                          else
                          begin
                          blank <= 1'b1;   //blank durumu
                          end
                    end 
              end
        end
       
     if (hCounter > hStartSync && hCounter <= hEndSync)
        vga_hsync <= hsync_active;
     else
        vga_hsync <= ~vsync_active;
     
     //  Are we in the vSync pulse?
     if (vCounter >= vStartSync && vCounter < vEndSync)
        vga_vsync <= vsync_active;
     else
        vga_vsync <= ~vsync_active;
              
  end

endmodule
