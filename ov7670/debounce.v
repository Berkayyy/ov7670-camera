`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.12.2021 19:53:16
// Design Name: 
// Module Name: debounce
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module debounce( input clk,
              input i,
              output reg o);

reg [23:0] c;

always@(posedge clk)
begin
    if (i == 1'b1)
    begin
        if (c == 24'hFFFFFF)
        begin
          o <= 1'b1;
        end
        else
        begin
          o <= 1'b0;
        end
        c <= c+1;
    end
    else
    begin
       c <= 0;
       o <= 1'b0;
    end
end
 

endmodule
